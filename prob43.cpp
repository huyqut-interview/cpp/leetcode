class Solution {
public:
    int char_to_int(char c) {
        return c - '0';
    }
    string multiply(string num1, string num2) {
        string product;
        int carry = 0;
        int start = 0;
        num1 = string(num1.rbegin(), num1.rend());
        num2 = string(num2.rbegin(), num2.rend());
        vector<int> raw_product;
        for (char a: num1) {
            int idx = start;
            for (char b: num2) {
                int num_a = char_to_int(a);
                int num_b = char_to_int(b);
                int sub_product = num_a * num_b;
                if (idx >= raw_product.size()) {
                    raw_product.push_back(0);
                }
                raw_product[idx] += carry + sub_product;
                if (raw_product[idx] >= 10) {
                    carry = raw_product[idx] / 10;
                    raw_product[idx] %= 10;
                } else {
                    carry = 0;
                }
                ++idx;
            }
            if (carry > 0) {
                raw_product.push_back(carry);
            }
            carry = 0;
            ++start;
        }
        product.reserve(raw_product.size());
        bool non_zero = false;
        for (int i = raw_product.size() - 1; i >= 0; --i) {
            if (non_zero || raw_product[i] != 0 || i == 0) {
                product.push_back(raw_product[i] + '0');
                if (!non_zero) {
                    non_zero = true;
                }
            }
        }
        return product;
    }
};
