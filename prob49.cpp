class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        auto anagrams = unordered_map<string, vector<string>>();
        for (auto& str: strs) {
            auto norm = str;
            sort(norm.begin(), norm.end());
            anagrams[norm].push_back(str);
        }
        vector<vector<string>> result;
        result.reserve(anagrams.size());
        for (auto& kv: anagrams) {
            result.push_back(kv.second);
        }
        return result;
    }
};
