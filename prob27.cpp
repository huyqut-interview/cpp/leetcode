class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int size = nums.size();
        int i = size - 1;
        for (; i >= 0 && nums[i] == val; --i);
        for (int j = 0; j < i; ++j) {
            for (; j < i && nums[j] != val; ++j);
            if (j == i) {
                break;
            }
            swap(nums[j], nums[i]);
            for (; i >= 0 && nums[i] == val; --i);
        }
        nums.resize(i + 1);
        return nums.size();
    }
};
