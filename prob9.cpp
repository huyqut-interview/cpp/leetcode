#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        int t = x;
        int y = 0;
        while (t != 0) {
            y = y * 10 + t % 10;
            t /= 10;
        }
        return y == x;
    }
};
