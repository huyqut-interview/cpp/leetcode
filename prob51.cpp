class Solution {
public:
    vector<vector<string>> solutions;
    vector<string> table;
    vector<bool> cols, major_diag, minor_diag;

    void solve_n_queens(int r) {
        if (r < 0) {
            solutions.push_back(table);
            return;
        }
        for (int c = 0; c < cols.size(); ++c) {
            int major = cols.size() - 1 - r + c;
            int minor = c + r;
            if (!cols[c] || !major_diag[major] || !minor_diag[minor]) {
                continue;
            }
            table[r][c] = 'Q';
            cols[c] = major_diag[major] = minor_diag[minor] = false;
            solve_n_queens(r - 1);
            cols[c] = major_diag[major] = minor_diag[minor] = true;
            table[r][c] = '.';
        }
    }

    vector<vector<string>> solveNQueens(int n) {
        table = vector<string>(n, string(n, '.'));
        cols = vector<bool>(n, true);
        major_diag = minor_diag = vector<bool>(2 * n - 1, true);
        solve_n_queens(n - 1);
        return solutions;
    }
};
