/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    int len(ListNode* node) {
        if (node == nullptr) {
            return 0;
        }
        int length = 0;
        do {
            ++length;
            node = node->next;
        } while (node != nullptr);
        return length;
    }
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        int length = len(head);
        auto dummy = new ListNode(0);
        dummy->next = head;
        int remain = length - n;
        auto prev = dummy;
        for (int i = 0; i < remain; ++i) {
            prev = prev->next;
        }
        auto removed = prev->next;
        prev->next = removed->next;
        delete removed;
        head = dummy->next;
        delete dummy;
        return head;
    }
};
