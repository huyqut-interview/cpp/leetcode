class Solution {
public:
    void reverse_subnums(vector<int>& nums, int a, int b) {
        while (a < b) {
            swap(nums[a], nums[b]);
            ++a;
            --b;
        }
    }
    void nextPermutation(vector<int>& nums) {
        int len = nums.size();
        if (len < 2) {
            return;
        }
        int i = len - 2;
        for (; i >= 0 && nums[i] >= nums[i + 1]; --i);
        if (i == -1) {
            reverse_subnums(nums, 0, len - 1);
            return;
        }
        int begin = i + 1;
        int end = len;
        int j = i + 1;
        while (begin < end) {
            int mid = begin + (end - begin) / 2;
            if (nums[mid] <= nums[i]) {
                end = mid;
            } else {
                j = mid;
                begin = mid + 1;
            }
        }
        swap(nums[i], nums[j]);
        reverse_subnums(nums, i + 1, len - 1);
    }
};
