class Solution {
public:
    vector<vector<bool>> picked;
    
    bool pattern_exists(int r, int c, int idx, const vector<vector<char>>& board, const string& word) {
        if (
            r == board.size() || r < 0 ||
            c == board[0].size() || c < 0 ||
            board[r][c] != word[idx] ||
            picked[r][c]
        ) {
            return false;
        }
        if (idx + 1 == word.length()) {
            return true;
        }
        picked[r][c] = true;
        bool result =
            pattern_exists(r + 1, c, idx + 1, board, word) ||
            pattern_exists(r, c + 1, idx + 1, board, word) ||
            pattern_exists(r - 1, c, idx + 1, board, word) ||
            pattern_exists(r, c - 1, idx + 1, board, word);
        picked[r][c] = false;
        return result;
    }
    
    bool exist(vector<vector<char>>& board, string word) {
        if (board.empty() || board[0].empty()) {
            return false;
        }
        int rows = board.size();
        int cols = board[0].size();
        picked.resize(rows, vector<bool>(cols, false));
        for (int r = 0; r < rows; ++r) {
            for (int c = 0; c < cols; ++c) {
                if (pattern_exists(r, c, 0, board, word)) {
                    return true;
                }
            }
        }
        return false;
    }
};
