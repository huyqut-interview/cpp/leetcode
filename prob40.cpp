class Solution {
public:
    void find_solution(unordered_map<int, int>::iterator it, unordered_map<int, int>::iterator end, int zero_sum, vector<int>& solution, vector<vector<int>>& solution_set) {
        if (zero_sum == 0) {
            solution_set.push_back(solution);
            return;
        }
        if (it == end || zero_sum < 0) {
            return;
        }
        int candidate = it->first;
        int frequency = it->second;
        if (frequency > 0) {
            --it->second;
            solution.push_back(candidate);
            find_solution(it, end, zero_sum - candidate, solution, solution_set);
            solution.pop_back();
            ++it->second;
        }
        find_solution(++it, end, zero_sum, solution, solution_set);
    }

    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        if (candidates.empty()) {
            return {};
        }
        vector<int> solution;
        vector<vector<int>> solution_set;
        auto candidate_frequency = unordered_map<int, int>();
        for (int candidate: candidates) {
            ++candidate_frequency[candidate];
        }
        find_solution(
            candidate_frequency.begin(),
            candidate_frequency.end(),
            target,
            solution,
            solution_set
        );
        return solution_set;
    }
};
