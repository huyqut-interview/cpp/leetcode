class Solution {
public:
    int lengthOfLastWord(string s) {
        int i = s.size() - 1;
        for (; i >= 0 && s[i] == ' '; --i);
        if (i < 0) {
            return 0;
        }
        int start = i;
        for (; i >= 0 && s[i] != ' '; --i);
        return start - i;
    }
};
