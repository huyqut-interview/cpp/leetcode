class Solution {
public:
    string countAndSay(int n) {
        if (n <= 0) {
            return "";
        }
        string result = "1";
        for (int i = 1; i < n; ++i) {
            string sub_result = "";
            int j = 0;
            int len = result.length();
            while (j < len) {
                char c = result[j];
                int start = j;
                for (++j; j < len && result[j] == c; ++j);
                int count = j - start;
                sub_result += string(1, count + '0') + string(1, c);
            }
            result = sub_result;
        }
        return result;
    }
};
