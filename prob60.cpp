class Solution {
public:
    string gen_permutation(int n, int k, int f, vector<int>& checked) {
        if (n == 0) {
            return "";
        }
        int multiple = (k - 1) / f;
        int m = 0;
        string digit;
        for (int i = 1; i < checked.size(); ++i) {
            if (checked[i]) {
                continue;
            }
            if (m == multiple) {
                checked[i] = true;
                digit = '0' + i;
                break;
            } else {
                ++m;
            }
        }
        if (n == 1) {
            return digit;
        }
        return digit + gen_permutation(n - 1, k - multiple * f, f / (n - 1), checked);
    }

    string getPermutation(int n, int k) {
        auto checked = vector<int>(n + 1, false);
        int f = 1;
        for (int i = 1; i < n; ++i) {
            f *= i;
        }
        return gen_permutation(n, k, f, checked);
    }
};
