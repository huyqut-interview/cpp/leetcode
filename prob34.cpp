class Solution {
public:
    int bin_search(const vector<int>& nums, int target, bool left) {
        int begin = 0;
        int end = nums.size();
        int idx = -1;
        while (begin < end) {
            int mid = begin + (end - begin) / 2;
            if (nums[mid] == target) {
                idx = mid;
                if (left) {
                    end = mid;
                } else {
                    begin = mid + 1;
                }
            } else if (nums[mid] < target) {
                begin = mid + 1;
            } else {
                end = mid;
            }
        }
        return idx;
    }
    
    vector<int> searchRange(vector<int>& nums, int target) {
        int left_most = bin_search(nums, target, true);
        if (left_most == -1) {
            return { -1, -1 };
        }
        int right_most = bin_search(nums, target, false);
        return {left_most, right_most};
    }
};
