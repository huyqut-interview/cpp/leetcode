class Solution {
public:
    void sortColors(vector<int>& nums) {
        int red_start = 0;
        int blue_start = nums.size() - 1;
        for (; red_start < blue_start && nums[red_start] == 0; ++red_start);
        for (; red_start < blue_start && nums[blue_start] == 2; --blue_start);
        for (int idx = red_start; idx <= blue_start;) {
            if (nums[idx] == 1) {
                ++idx;
            } else {
                if (nums[idx] == 0) {
                    swap(nums[idx], nums[red_start]);
                    for (++red_start; red_start < blue_start && nums[red_start] == 0; ++red_start);
                } else if (nums[idx] == 2) {
                    swap(nums[idx], nums[blue_start]);
                    for (--blue_start; red_start < blue_start && nums[blue_start] == 2; --blue_start);
                }
                idx = red_start;
            }
        }
    }
};
