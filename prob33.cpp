class Solution {
public:
    int search(vector<int>& nums, int target) {
        if (nums.empty()) {
            return -1;
        }
        if (nums.size() == 1) {
            return nums[0] == target ? 0 : -1;
        }
        int begin = 0;
        int end = nums.size();
        int pivot = 0;
        while (begin < end) {
            int mid = begin + (end - begin) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            if (nums[mid] > nums[begin]) {
                if (nums[mid] <= nums[end - 1]) {
                    pivot = begin;
                    break;
                }
                begin = mid + 1;
            } else if (nums[mid] < nums[begin]) {
                if (end - begin == 2) {
                    pivot = end - 1;
                    break;
                }
                end = mid + 1;
            } else {
                pivot = begin;
                break;
            }
        }
        begin = 0;
        end = nums.size();
        while (begin < end) {
            int mid = begin + (end - begin) / 2;
            int shift_mid = (mid + pivot) % nums.size();
            if (nums[shift_mid] == target) {
                return shift_mid;
            } else if (nums[shift_mid] < target) {
                begin = mid + 1;
            } else {
                end = mid;
            }
        }
        return -1;
    }
};
