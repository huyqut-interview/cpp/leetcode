/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {
        if (head == nullptr) {
            return nullptr;
        }

        int length = 0;
        for (auto lt = head; lt != nullptr; lt = lt->next) {
            ++length;
        }
        k = k % length;
        if (k == 0) {
            return head;
        }
        k = length - k;

        auto dummy = new ListNode(0);
        dummy->next = head;
        auto it = dummy;
        for (; k > 0; --k, it = it->next);
        auto jt = it;
        for (; jt->next != nullptr; jt = jt->next);

        jt->next = dummy->next;
        dummy->next = it->next;
        it->next = nullptr;
        auto result = dummy->next;
        delete dummy;

        return result;
    }
};
