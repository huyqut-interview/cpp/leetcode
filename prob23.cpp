/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
#include <bits/stdc++.h>

using namespace std;

struct ValIdx {
    int val;
    int idx;
    ValIdx(): val(0), idx(0) {}
    ValIdx(int val, int idx): val(val), idx(idx) {}
};

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        int size = lists.size();
        auto dummies = vector<ListNode*>(size, nullptr);
        auto compare = [](const ValIdx& a, const ValIdx& b) {
            return a.val > b.val;
        };
        priority_queue<ValIdx, vector<ValIdx>, decltype(compare)> min_heap(compare);
        for (int i = 0; i < size; ++i) {
            dummies[i] = new ListNode(0);
            dummies[i]->next = lists[i];
            if (lists[i] != nullptr) {
                min_heap.push(ValIdx(lists[i]->val, i));
            }
        }
        auto dummy = new ListNode(0);
        auto it = dummy;
        while (!min_heap.empty()) {
            auto top = min_heap.top();
            it->next = dummies[top.idx]->next;
            it = it->next;
            if (it != nullptr) {
                dummies[top.idx]->next = it->next;
                if (it->next != nullptr) {
                    min_heap.push(ValIdx(it->next->val, top.idx));
                }
            }
            min_heap.pop();
        }
        for (int i = 0; i < size; ++i) {
            delete dummies[i];
        }
        it = dummy->next;
        delete dummy;
        return it;
    }
};
