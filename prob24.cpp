/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        auto dummy = new ListNode(0);
        dummy->next = head;
        auto it = dummy;
        while (it != nullptr) {
            auto a = it->next;
            if (a == nullptr) {
                break;
            }
            auto b = a->next;
            if (b == nullptr) {
                break;
            }
            it->next = b;
            a->next = b->next;
            b->next = a;
            it = a;
        }
        it = dummy->next;
        delete dummy;
        return it;
    }
};
