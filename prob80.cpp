class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int size = nums.size();
        if (size < 3) {
            return size;
        }

        int start = 1;
        for (int idx = 2; idx < size; ++idx) {
            if (
                nums[idx] == nums[start] &&
                nums[start] == nums[start - 1]
            ) {
                continue;
            }
            ++start;
            nums[start] = nums[idx];
        }

        nums.resize(start + 1);
        return start + 1;
    }
};
