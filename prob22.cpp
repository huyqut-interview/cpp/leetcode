class Solution {
public:
    vector<string> solution;
    
    void gen_par(int open_count, int close_count, string& sub_solution) {
        if (close_count == 0) {
            solution.push_back(sub_solution);
            return;
        }
        if (open_count > 0) {
            sub_solution += '(';
            gen_par(open_count - 1, close_count, sub_solution);
            sub_solution.pop_back();
        }
        if (close_count > open_count) {
            sub_solution += ')';
            gen_par(open_count, close_count - 1, sub_solution);
            sub_solution.pop_back();
        }
    }
    
    vector<string> generateParenthesis(int n) {
        string sub_solution;
        sub_solution.reserve(2 * n + 1);
        gen_par(n, n, sub_solution);
        return solution;
    }
};
