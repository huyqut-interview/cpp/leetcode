class Solution {
public:
    vector<vector<int>> solution;
    vector<int> sub_solution;

    void generate_permutation(int idx) {
        if (idx == sub_solution.size()) {
            solution.push_back(sub_solution);
            return;
        }
        for (int i = idx; i < sub_solution.size(); ++i) {
            swap(sub_solution[idx], sub_solution[i]);
            generate_permutation(idx + 1);
            swap(sub_solution[idx], sub_solution[i]);
        }
    }
    
    vector<vector<int>> permute(vector<int>& nums) {
        int expected_size = 1;
        for (int i = nums.size(); i > 1; --i) {
            expected_size *= i;
        }
        solution.reserve(expected_size);
        sub_solution = nums;
        generate_permutation(0);
        return solution;
    }
};
