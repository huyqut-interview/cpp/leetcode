class Solution {
public:

    int jump(vector<int>& nums) {
        if (nums.empty()) {
            return 0;
        }

        int max_jump = 0;
        int pivot = 0;
        for (int result = 0; result < nums.size(); ++result) {
            if (max_jump + 1 >= nums.size()) {
                return result;
            }
            int current_jump = max_jump;
            for (; pivot <= current_jump && max_jump + 1 < nums.size(); ++pivot) {
                int sub_jump = nums[pivot];
                max_jump = max(max_jump, pivot + sub_jump);
            }
        }
        return 0;
    }
};
