class Solution {
public:
    vector<vector<int>> table;
    int is_match(int str_idx, int reg_idx, const string& str, const string& reg) {
        if (str_idx == str.length()) {
            if (reg_idx == reg.length()) {
                return 1;
            } else if (reg[reg_idx] == '*') {
                return is_match(str_idx, reg_idx + 1, str, reg);
            } else {
                return 0;
            }
        }
        if (reg_idx == reg.length()) {
            return 0;
        }
        if (table[str_idx][reg_idx] != -1) {
            return table[str_idx][reg_idx];
        }
        if (reg[reg_idx] == '?') {
            table[str_idx][reg_idx] = is_match(str_idx + 1, reg_idx + 1, str, reg);
        } else if (reg[reg_idx] == '*') {
            int result = is_match(str_idx + 1, reg_idx + 1, str, reg);
            if (result != 1) {
                result = is_match(str_idx + 1, reg_idx, str, reg);
                if (result != 1) {
                    result = is_match(str_idx, reg_idx + 1, str, reg);
                }
            }
            table[str_idx][reg_idx] = result;
        } else {
            if (str[str_idx] != reg[reg_idx]) {
                table[str_idx][reg_idx] = 0;
            } else {
                table[str_idx][reg_idx] = is_match(str_idx + 1, reg_idx + 1, str, reg);
            }
        }
        return table[str_idx][reg_idx];
    }
    bool isMatch(string s, string p) {
        for (int i = 0; i <= s.size(); ++i) {
            table.push_back(vector<int>(p.size() + 1, -1));
        }
        return is_match(0, 0, s, p);
    }
};
