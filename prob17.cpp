#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    vector<string> solution;
    vector<string> tpad = {
        "",
        "",
        "abc",
        "def",
        "ghi",
        "jkl",
        "mno",
        "pqrs",
        "tuv",
        "wxyz"
    };
    
    void combine(int idx, const string& digits, string& sub_solution) {
        if (idx == digits.size()) {
            solution.push_back(sub_solution);
            return;
        }
        const auto& pad = tpad[digits[idx] - '0'];
        for (char c: pad) {
            sub_solution.push_back(c);
            combine(idx + 1, digits, sub_solution);
            sub_solution.pop_back();
        }
        return;
    }
    
    vector<string> letterCombinations(string digits) {
        if (digits.empty()) {
            return {};
        }
        int size = digits.size();
        string sub_solution;
        sub_solution.reserve(size);
        combine(0, digits, sub_solution);
        return solution;
    }
};
