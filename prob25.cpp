/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
        auto dummy = new ListNode(0);
        dummy->next = head;
        auto it = dummy;
        while (true) {
            auto end = it;
            for (int i = 0; i < k && end != nullptr; ++i) {
                end = end->next;
            }
            if (end == nullptr) {
                break;
            }
            auto next_part = end->next;
            end->next = nullptr;
            auto next_it = it->next;
            auto prev = it;
            auto jt = next_it;
            while (jt != nullptr) {
                auto next = jt->next;
                jt->next = prev;
                prev = jt;
                jt = next;
            }
            it->next = prev;
            it = next_it;
            it->next = next_part;
        }
        auto result = dummy->next;
        delete dummy;
        return result;
    }
};
