class Solution {
public:
    vector<int> subset;
    vector<vector<int>> result;

    void find_subset(int idx, const vector<int>& nums) {
        if (idx == nums.size()) {
            return;
        }
        subset.push_back(nums[idx]);
        result.emplace_back(subset);
        find_subset(idx + 1, nums);
        subset.pop_back();
        find_subset(idx + 1, nums);
    }
    
    vector<vector<int>> subsets(vector<int>& nums) {
        find_subset(0, nums);
        result.push_back({});
        return result;
    }
};
