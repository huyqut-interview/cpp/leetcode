class Solution {
public:

    vector<vector<int>> dp;

    int min_edit_distance(
        int source_idx, int target_idx,
        const string& source, const string& target
    ) {
        if (source_idx < 0 && target_idx < 0) {
            return 0;
        }
        if (source_idx < 0) {
            return target_idx + 1;
        }
        if (target_idx < 0) {
            return source_idx + 1;
        }
        if (dp[source_idx][target_idx] != -1) {
            return dp[source_idx][target_idx];
        }
        if (source[source_idx] == target[target_idx]) {
            dp[source_idx][target_idx] = min_edit_distance(
                source_idx - 1, target_idx - 1,
                source, target
            );
        } else {
            dp[source_idx][target_idx] = min({
                // Insert a char to source = the char at target
                1 + min_edit_distance(source_idx, target_idx - 1, source, target),
                // Delete the char at source that mismatches the target
                1 + min_edit_distance(source_idx - 1, target_idx, source, target),
                // Replace the char at source to match with the target
                1 + min_edit_distance(source_idx - 1, target_idx - 1, source, target)
            });
        }
        return dp[source_idx][target_idx];
    }
    
    int minDistance(string word1, string word2) {
        dp.resize(word1.size(), vector<int>(word2.size(), -1));
        return min_edit_distance(
            word1.size() - 1, word2.size() - 1,
            word1, word2
        );
    }
};
