class Window {
public:
    int left_idx;
    int right_idx;
    string source;
    string target;
    int length;
    unordered_map<char, int> char_count;

    Window(const string& source, const string& target):
        source(source), target(target), left_idx(0), right_idx(-1) {
        length = target.length();
        for (char c: target) {
            ++char_count[c];
        }
        expand_right();
    }

    bool match() {
        return length == 0;
    }

    bool expandable() {
        return left_idx <= right_idx && right_idx < source.length();
    }

    void contract_left() {
        if (left_idx >= source.length()) {
            return;
        }
        if (char_count.find(source[left_idx]) != char_count.end()) {
            ++char_count[source[left_idx]];
            if (char_count[source[left_idx]] > 0) {
                ++length;
            }
        }
        ++left_idx;
    }

    void expand_right() {
        ++right_idx;
        if (
            right_idx >= source.length() ||
            char_count.find(source[right_idx]) == char_count.end()
        ) {
            return;
        }
        if (char_count[source[right_idx]] > 0) {
            --length;
        }
        --char_count[source[right_idx]];
    }

    tuple<int, int> spec() {
        return make_tuple(left_idx, right_idx - left_idx + 1);
    }
};

class Solution {
public:
    string minWindow(string s, string t) {
        if (s.empty() || t.empty()) {
            return "";
        }
        int src_len = s.length();
        int tar_len = t.length();
        if (tar_len > src_len) {
            return "";
        }

        Window window(s, t);
        while (window.expandable() && !window.match()) {
            window.expand_right();
        }
        if (!window.match()) {
            cout << "oop";
            return "";
        }
        int min_win_start, min_win_length;
        tie(min_win_start, min_win_length) = window.spec();

        while (window.expandable()) {
            if (!window.match()) {
                window.expand_right();
            } else {
                int new_win_start, new_win_length;
                tie(new_win_start, new_win_length) = window.spec();
                if (new_win_length < min_win_length) {
                    min_win_start = new_win_start;
                    min_win_length = new_win_length;
                }
                window.contract_left();
            }
        }
        return s.substr(min_win_start, min_win_length);
    }
};
