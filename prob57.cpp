/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */

bool compare(const Interval& a, const Interval& b) {
    return a.start != b.start ?
        a.start < b.start :
        a.end < b.end;
}

class Solution {
public:
    vector<Interval> insert(vector<Interval>& intervals, Interval newInterval) {
        if (intervals.empty()) {
            return { newInterval };
        }
        sort(intervals.begin(), intervals.end(), compare);
        int end = 0;
        for (; end < intervals.size(); ++end) {
            if (newInterval.end < intervals[end].start) {
                break;
            }
        }
        int start = end - 1;
        for (; start >= 0; --start) {
            if (newInterval.start > intervals[start].end) {
                break;
            }
        }
        if (start + 1 == end) {
            // New interval
            intervals.push_back(intervals.back());
            for (int i = intervals.size() - 2; i >= 1 && i > start; --i) {
                intervals[i] = intervals[i - 1];
            }
            intervals[end] = newInterval;
        } else {
            // Insert new one
            intervals[start + 1].start = min(intervals[start + 1].start, newInterval.start);
            intervals[start + 1].end = max(intervals[end - 1].end, newInterval.end);
            // Compact intervals
            int diff = end - start - 2;
            int i = start + 2;
            for (; i + diff < intervals.size(); ++i) {
                intervals[i] = intervals[i + diff];
            }
            intervals.resize(i);
        }
        return intervals;
    }
};
