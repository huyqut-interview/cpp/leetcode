#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        vector<vector<int>> solution;
        for (int a = 0; a < nums.size(); ) {
            for (int b = a + 1; b < nums.size(); ) {
                int u = b + 1;
                int v = nums.size() - 1;
                int sub_total = nums[a] + nums[b];
                while (u < v) {
                    int total = sub_total + nums[u] + nums[v];
                    if (total == target) {
                        solution.push_back({ nums[a], nums[b], nums[u], nums[v] });
                        do {
                            ++u;
                        } while (u < v && nums[u] == nums[u - 1]);
                        do {
                            --v;
                        } while (u < v && nums[v] == nums[v + 1]);
                    } else if (total < target) {
                        ++u;
                    } else {
                        --v;
                    }
                }
                do {
                    ++b;
                } while (b < nums.size() && nums[b] == nums[b - 1]);
            }
            do {
                ++a;
            } while (a < nums.size() && nums[a] == nums[a - 1]);
        }
        return solution;
    }
};
