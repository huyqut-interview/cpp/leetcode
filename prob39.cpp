#include <algorithm>

using namespace std;

class Solution {
public:
    vector<vector<int>> solution_set;

    void permute_sum(int index, int sum, int target, vector<int>& solution, const vector<int>& candidates) {
        if (sum >= target) {
            if (sum == target) {
                solution_set.push_back(solution);
            }
            return;
        }
        if (index >= candidates.size()) {
            return;
        }
        solution.push_back(candidates[index]);
        permute_sum(index, sum + candidates[index], target, solution, candidates);
        solution.pop_back();
        permute_sum(index + 1, sum, target, solution, candidates);
    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        sort(candidates.begin(), candidates.end());
        vector<int> solution;
        permute_sum(0, 0, target, solution, candidates);
        return solution_set;
    }
};
