class Solution {
public:
    int get_val(vector<vector<int>>& matrix, int idx) {
        return matrix[idx / matrix[0].size()][idx % matrix[0].size()];
    }
    
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        if (matrix.empty() || matrix[0].empty()) {
            return false;
        }
        int rows = matrix.size();
        int cols = matrix[0].size();
        int begin = 0;
        int end = rows * cols;
        while (begin < end) {
            int mid_idx = begin + (end - begin) / 2;
            int mid_val = get_val(matrix, mid_idx);
            if (target == mid_val) {
                return true;
            }
            if (mid_val < target) {
                begin = mid_idx + 1;
            } else {
                end = mid_idx;
            }
        }
        return false;
    }
};
