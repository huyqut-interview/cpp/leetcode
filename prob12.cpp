#include <bits/stdc++.h>

using namespace std;

struct Roman {
    char rom;
    int val;
    Roman(char rom, int val): rom(rom), val(val) {}
};

class Solution {
public:
    const vector<Roman> romans = {
        Roman('I', 1),
        Roman('V', 5),
        Roman('X', 10),
        Roman('L', 50),
        Roman('C', 100),
        Roman('D', 500),
        Roman('M', 1000)
    };
    string intToRoman(int num) {
        string roman_str;
        roman_str.reserve(10);
        for (int i = romans.size() - 1; i > 0; --i) {
            const auto& pre_one = romans[i - 2 + i % 2];
            int quotient = num / romans[i].val;
            if (quotient > 0) {
                roman_str += string(quotient, romans[i].rom);
                num %= romans[i].val;
            }
            if (num / pre_one.val == (romans[i].val - pre_one.val) / pre_one.val) {
                roman_str += string(1, pre_one.rom) + string(1, romans[i].rom);
                num -= romans[i].val - pre_one.val;
            }
        }
        if (num > 0) {
            roman_str += string(num, 'I');
        }
        return roman_str;
    }
};
