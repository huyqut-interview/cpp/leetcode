#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        vector<vector<int>> solution;
        for (int i = 0; i < nums.size();) {
            if (nums[i] > 0) {
                break;
            }
            int j = i + 1;
            int k = nums.size() - 1;
            while (j < k) {
                int total = nums[i] + nums[j] + nums[k];
                if (total == 0) {
                    solution.push_back({ nums[i], nums[j], nums[k] });
                    do {
                        ++j;
                    } while (j < k && nums[j] == nums[j - 1]);
                    do {
                        --k;
                    } while (j < k && nums[k] == nums[k + 1]);
                } else if (total < 0) {
                    ++j;
                } else {
                    --k;
                }
            }
            do {
                ++i;
            } while (i < nums.size() && nums[i] == nums[i - 1]);
        }
        return solution;
    }
};
