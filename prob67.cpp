class Solution {
public:
    int get_bin_digit(int idx, const string& bin) {
        if (idx >= bin.length()) {
            return 0;
        }
        return bin[bin.length() - idx - 1] - '0';
    }
    string addBinary(string a, string b) {
        string reverse_result;
        int length = max(a.length(), b.length());
        int carry = 0;
        for (int idx = 0; idx < length; ++idx) {
            int sum = carry + get_bin_digit(idx, a) + get_bin_digit(idx, b);
            carry = sum / 2;
            reverse_result.push_back(sum % 2 + '0');
        }
        if (carry == 1) {
            reverse_result.push_back('1');
        }
        return string(reverse_result.rbegin(), reverse_result.rend());
    }
};
