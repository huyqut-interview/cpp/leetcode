class Solution {
public:
    vector<int> comb;
    vector<vector<int>> result;

    void sub_combine(int index, int size, int num, int max_num) {
        if (index == size) {
            result.emplace_back(comb);
            return;
        }
        if (num > max_num) {
            return;
        }
        comb[index] = num;
        sub_combine(index + 1, size, num + 1, max_num);
        sub_combine(index, size, num + 1, max_num);
    }
    
    vector<vector<int>> combine(int n, int k) {
        comb.resize(k, 0);
        sub_combine(0, k, 1, n);
        return result;
    }
};
