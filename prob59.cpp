class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        auto matrix = vector<vector<int>>(n, vector<int>(n, 0));
        int size = n * n;
        int r = 0, c = -1;
        int direction = 0;
        int upper = 1, lower = n - 1, left = 0, right = n - 1;
        for (int i = 0; i < size;) {
            switch (direction) {
                case 0:
                    if (c == right) {
                        direction = 1;
                        --right;
                        continue;
                    }
                    ++c;
                    break;
                case 1:
                    if (r == lower) {
                        direction = 2;
                        --lower;
                        continue;
                    }
                    ++r;
                    break;
                case 2:
                    if (c == left) {
                        direction = 3;
                        ++left;
                        continue;
                    }
                    --c;
                    break;
                case 3:
                    if (r == upper) {
                        direction = 0;
                        ++upper;
                        continue;
                    }
                    --r;
                    break;
            }
            ++i;
            matrix[r][c] = i;
        }
        return matrix;
    }
};
