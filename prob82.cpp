/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    void remove_by_value(ListNode* prev, int value) {
        if (prev == nullptr) {
            return;
        }
        while (prev->next != nullptr && prev->next->val == value) {
            auto del = prev->next;
            prev->next = del->next;
            delete del;
        }
    }
    
    ListNode* deleteDuplicates(ListNode* head) {
        auto dummy = new ListNode(0);
        dummy->next = head;

        auto it = dummy;
        while (it->next != nullptr) {
            auto jt = it->next;
            if (jt->next == nullptr) {
                break;
            }
            if (jt->val == jt->next->val) {
                remove_by_value(it, jt->val);
            } else {
                it = it->next;
            }
        }

        auto result = dummy->next;
        delete dummy;
        return result;
    }
};
