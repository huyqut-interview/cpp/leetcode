#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        auto size = nums.size();
        int diff = INT_MAX;
        int result = 0;
        for (int i = 0; i < size;) {
            int j = i + 1;
            int k = size - 1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum == target) {
                    return target;
                }
                int new_diff = abs(target - sum);
                if (new_diff < diff) {
                    result = sum;
                    diff = new_diff;
                }
                if (sum < target) {
                    do {
                        ++j;
                    } while (j < k && nums[j] == nums[j - 1]);
                } else {
                    do {
                        --k;
                    } while (j < k && nums[k] == nums[k + 1]);
                }
            }
            do {
                ++i;
            } while (i < size && nums[i] == nums[i - 1]);
        }
        return result;
    }
};
