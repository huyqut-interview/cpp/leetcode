/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */

bool compare(const Interval& a, const Interval& b) {
    return a.start != b.start ?
        a.start < b.start :
        a.end < b.end;
}

class Solution {
public:
    
    vector<Interval> merge(vector<Interval>& intervals) {
        if (intervals.size() <= 1) {
            return intervals;
        }
        sort(intervals.begin(), intervals.end(), compare);
        auto result = vector<Interval>();
        result.reserve(intervals.size());
        int i = 0, j = 0;
        while (i < intervals.size()) {
            int k = intervals[i].end;
            for (++j; j < intervals.size(); ++j) {
                if (k < intervals[j].start) {
                    break;
                }
                k = max(k, intervals[j].end);
            }
            result.push_back(Interval(intervals[i].start, k));
            i = j;
        }
        return result;
    }
};
