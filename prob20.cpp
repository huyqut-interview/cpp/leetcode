#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    bool isValid(string s) {
        vector<char> par;
        par.reserve(s.size() + 1);
        unordered_map<char, char> par_pair = {
            { ')', '(' },
            { '}', '{' },
            { ']', '[' },
        };
        for (char c: s) {
            if (
                c == '(' ||
                c == '{' ||
                c == '['
            ) {
                par.push_back(c);
            } else if (par_pair[c] != par.back()) {
                return false;
            } else {
                par.pop_back();
            }
        }
        return par.empty();
    }
};
