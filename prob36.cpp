class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        auto rows = vector<unordered_set<int>>(9, unordered_set<int>({}));
        auto cols = vector<unordered_set<int>>(9, unordered_set<int>({}));
        auto squr = vector<unordered_set<int>>(9, unordered_set<int>({}));
        for (int r = 0; r < 9; ++r) {
            for (int c = 0; c < 9; ++c) {
                if (board[r][c] == '.') {
                    continue;
                }
                int value = board[r][c] - '0';
                int x = r / 3 * 3 + c / 3;
                if (
                    rows[r].find(value) != rows[r].end() ||
                    cols[c].find(value) != cols[c].end() ||
                    squr[x].find(value) != squr[x].end()
                ) {
                    return false;
                }
                rows[r].insert(value);
                cols[c].insert(value);
                squr[x].insert(value);
            }
        }
        return true;
    }
};
