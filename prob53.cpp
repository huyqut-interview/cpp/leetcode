class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int sum = 0;
        int min_sum = 0;
        int max_sum = nums[0];
        for (auto num: nums) {
            sum += num;
            max_sum = max(max_sum, sum - min_sum);
            min_sum = min(min_sum, sum);
        }
        return max_sum;
    }
};
