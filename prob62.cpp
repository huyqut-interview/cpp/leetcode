class Solution {
public:
    int uniquePaths(int m, int n) {
        auto column = vector<int>(n, 1);
        for (int i = 1; i < m; ++i) {
            for (int j = 1; j < n; ++j) {
                column[j] += column[j - 1];
            }
        }
        return column.back();
    }
};
