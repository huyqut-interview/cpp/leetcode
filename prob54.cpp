class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        if (matrix.empty() || matrix[0].empty()) {
            return {};
        }
        auto result = vector<int>();
        int row = matrix.size();
        int col = matrix[0].size();
        int flat_size = row * col;
        result.reserve(flat_size);
        int r = 0, c = -1;
        int direction = 0;
        int upper = 1, lower = row - 1, left = 0, right = col - 1;
        for (int i = 0; i < flat_size;) {
            switch (direction) {
                case 0:
                    if (c == right) {
                        direction = 1;
                        --right;
                        continue;
                    }
                    ++c;
                    break;
                case 1:
                    if (r == lower) {
                        direction = 2;
                        --lower;
                        continue;
                    }
                    ++r;
                    break;
                case 2:
                    if (c == left) {
                        direction = 3;
                        ++left;
                        continue;
                    }
                    --c;
                    break;
                case 3:
                    if (r == upper) {
                        direction = 0;
                        ++upper;
                        continue;
                    }
                    --r;
                    break;
            }
            result.push_back(matrix[r][c]);
            ++i;
        }
        return result;
    }
};
