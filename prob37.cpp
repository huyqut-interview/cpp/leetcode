class Solution {
public:
    vector<vector<bool>> rows;
    vector<vector<bool>> cols;
    vector<vector<bool>> squr;
    
    bool solve_sudoku(int r, int c, vector<vector<char>>& board) {
        if (r >= 9) {
            return true;
        }
        if (c >= 9) {
            return solve_sudoku(r + 1, 0, board);
        }
        if (board[r][c] != '.') {
            return solve_sudoku(r, c + 1, board);
        }
        int q = r / 3 * 3 + c / 3;
        for (int i = 1; i <= 9; ++i) {
            if (rows[r][i] && cols[c][i] && squr[q][i]) {
                rows[r][i] = false;
                cols[c][i] = false;
                squr[q][i] = false;
                board[r][c] = i + '0';
                if (solve_sudoku(r, c + 1, board)) {
                    return true;
                }
                board[r][c] = '.';
                rows[r][i] = true;
                cols[c][i] = true;
                squr[q][i] = true;
            }
        }
        return false;
    }
    
    void solveSudoku(vector<vector<char>>& board) {
        rows.resize(9, vector<bool>(10, true));
        cols.resize(9, vector<bool>(10, true));
        squr.resize(9, vector<bool>(10, true));
        for (int r = 0; r < 9; ++r) {
            for (int c = 0; c < 9; ++c) {
                if (board[r][c] == '.') {
                    continue;
                }
                int v = board[r][c] - '0';
                int q = r / 3 * 3 + c / 3;
                rows[r][v] = false;
                cols[c][v] = false;
                squr[q][v] = false;
            }
        }
        solve_sudoku(0, 0, board);
    }
};
