class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int begin = 0;
        int end = nums.size();
        int mid = end;
        while (begin < end) {
            mid = begin + (end - begin) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] < target) {
                begin = mid + 1;
            } else {
                end = mid;
            }
        }
        if (mid != end) {
            if (nums[mid] < target) {
                ++mid;
            }
        }
        return mid;
    }
};
