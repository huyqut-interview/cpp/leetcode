class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        if (matrix.empty()) {
            return;
        }
        int size = matrix.size();
        int half = size / 2;
        int odd = size % 2;
        for (int r = 0; r < half + odd; ++r) {
            for (int c = 0; c < half; ++c) {
                int d =  matrix[r][c];
                matrix[r][c] = matrix[size - c - 1][r];
                matrix[size - c - 1][r] = matrix[size - r - 1][size - c - 1];
                matrix[size - r - 1][size - c - 1] = matrix[c][size - r - 1];
                matrix[c][size - r - 1] = d;
            }
        }
    }
};
