#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    
    vector<int> make_failure(const string& needle) {
        if (needle.empty()) {
            return {};
        }
        int len = needle.length();
        vector<int> failure(len, 0);
        failure[0] = -1;
        int i = 0;
        for (int j = 1; j < len; ++j) {
            if (needle[i] != needle[j]) {
                failure[j] = i;
                i = failure[i];
                if (i == -1) {
                    i = 0;
                } else {
                    ++i;
                }
            } else {
                failure[j] = failure[i];
                ++i;
            }
        }
        return failure;
    }
    
    int strStr(string haystack, string needle) {
        int hay_len = haystack.length();
        int nee_len = needle.length();
        if (nee_len > hay_len) {
            return -1;
        }
        if (nee_len == hay_len) {
            return haystack == needle ? 0 : -1;
        }
        auto failure = make_failure(needle);
        int j = 0;
        for (int i = 0; i < hay_len; ++i) {
            if (j == nee_len) {
                return i - j;
            }
            if (haystack[i] == needle[j]) {
                ++j;
                continue;
            }
            for (; j != -1 && haystack[i] != needle[j]; j = failure[j]);
            if (j == -1) {
                j = 0;
            } else {
                ++j;
            }
        }
        return j == nee_len ? hay_len - nee_len : -1;
    }
};
