class Solution {
public:
    int mySqrt(int x) {
        if (x == 0) {
            return x;
        }
        int begin = 1;
        int end = x / 2 + 1;
        while (begin < end) {
            int mid = begin + (end - begin) / 2;
            int other_mid = x / mid;
            if (mid == other_mid && mid * other_mid == x) {
                return mid;
            }
            if (mid < other_mid) {
                begin = mid + 1;
            } else {
                end = mid;
            }
        }
        return x / begin >= begin ? begin : begin - 1;
    }
};
