class Solution {
public:
    vector<vector<string>> solutions;
    vector<bool> cols, major_diag, minor_diag;

    int solve_n_queens(int r) {
        if (r < 0) {
            return 1;
        }
        int total = 0;
        for (int c = 0; c < cols.size(); ++c) {
            int major = cols.size() - 1 - r + c;
            int minor = c + r;
            if (!cols[c] || !major_diag[major] || !minor_diag[minor]) {
                continue;
            }
            cols[c] = major_diag[major] = minor_diag[minor] = false;
            total += solve_n_queens(r - 1);
            cols[c] = major_diag[major] = minor_diag[minor] = true;
        }
        return total;
    }

    int totalNQueens(int n) {
        cols = vector<bool>(n, true);
        major_diag = minor_diag = vector<bool>(2 * n - 1, true);
        return solve_n_queens(n - 1);
    }
};
