class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& grid) {
        if (grid.empty() || grid[0].empty()) {
            return 0;
        }
        int rows = grid.size();
        int cols = grid[0].size();
        auto table = vector<vector<long long>>(rows, vector<long long>(cols, 0));
        for (int r = 0; r < rows && grid[r][0] == 0; ++r) {
            table[r][0] = 1;
        }
        for (int c = 0; c < cols && grid[0][c] == 0; ++c) {
            table[0][c] = 1;
        }
        for (int c = 1; c < cols; ++c) {
            for (int r = 1; r < rows; ++r) {
                if (grid[r][c] == 1) {
                    continue;
                }
                table[r][c] = table[r - 1][c] + table[r][c - 1];
            }
        }
        return table.back().back();
    }
};
