class Solution {
public:
    int climbStairs(int n) {
        int pre_1_step = 1;
        int pre_2_steps = 0;
        int steps = 0;
        for (; n > 0; --n) {
            steps = pre_1_step + pre_2_steps;
            pre_2_steps = pre_1_step;
            pre_1_step = steps;
        }
        return steps;
    }
};
