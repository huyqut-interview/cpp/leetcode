#include <bits/stdc++.h>

using namespace std;

unordered_map<char, int> romans = {
    { 'I', 1 },
    { 'V', 5 },
    { 'X', 10 },
    { 'L', 50 },
    { 'C', 100 },
    { 'D', 500 },
    { 'M', 1000 }
};

class Solution {
public:

    int romanToInt(string s) {
        if (s.empty()) {
            return 0;
        
        }
        int value = 0;
        int idx = s.size() - 1;
        value += romans[s[idx]];
        --idx;
        for (; idx >= 0; --idx) {
            if (romans[s[idx]] < romans[s[idx + 1]]) {
                value -= romans[s[idx]];
            } else {
                value += romans[s[idx]];
            }
        }
        return value;
    }
};
