class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        int size = digits.size();
        if (size == 0) {
            return {};
        }
        int carry = 1;
        int idx = size - 1;
        for (; idx >= 0 && carry == 1; --idx) {
            int sum = digits[idx] + carry;
            carry = sum / 10;
            digits[idx] = sum % 10;
        }
        if (carry == 1) {
            digits.push_back(0);
            for (int i = size; i > 0; --i) {
                digits[i] = digits[i - 1];
            }
            digits[0] = 1;
        }
        return digits;
    }
};
