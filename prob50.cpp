class Solution {
public:
    double power_up(int n, double y) {
        if (n == 0) {
            return 1;
        }
        if (n < 0) {
            if (n == -2147483648) {
                return (power_up(n + 1, y)) / y;
            }
            return 1 / power_up(-n, y);
        }
        double result = power_up(n >> 1, y * y);
        if (n & 1 == 1) {
            result *= y;
        }
        return result;
    }
    
    double myPow(double x, int n) {
        return power_up(n, x);
    }
};
