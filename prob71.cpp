class Solution {
public:
    void push(deque<string>& canon_path, const string& element) {
        canon_path.push_back(element);
    }

    void pop(deque<string>& canon_path) {
        if (canon_path.empty()) {
            return;
        }
        canon_path.pop_back();
    }

    int next_slash_idx(int start_idx, const string& abs_path) {
        int length = abs_path.length();
        int idx = start_idx;
        for (; idx < length && abs_path[idx] != '/'; ++idx);
        return idx;
    }

    vector<string> tokenize_abs_path(const string& abs_path) {
        int idx = 0;
        int length = abs_path.length();
        vector<string> tokens;
        while (idx < length) {
            if (abs_path[idx] == '/') {
                ++idx;
                continue;
            }
            int end_token_idx = next_slash_idx(idx, abs_path);
            tokens.push_back(abs_path.substr(idx, end_token_idx - idx));
            idx = end_token_idx + 1;
        }
        return tokens;
    }

    string simplifyPath(string abs_path) {
        auto tokens = tokenize_abs_path(abs_path);
        deque<string> canon_path;
        for (const string& token: tokens) {
            if (token == ".") {
                continue;
            }
            if (token == "..") {
                pop(canon_path);
            } else {
                push(canon_path, token);
            }
        }
        if (canon_path.empty()) {
            return "/";
        }
        string canon_path_str;
        while (!canon_path.empty()) {
            canon_path_str += "/" + canon_path.front();
            canon_path.pop_front();
        }
        return canon_path_str;
    }
};
